package craps;

import com.itver.proyect.resources.NetConnection;
import com.itver.proyect.resources.PacketNet;
import com.itver.proyect.resources.Protocol;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Juego extends javax.swing.JFrame {

    private final NetConnection connection;
    private String usuarioRival;
    private final String usuario;
    private int tiroRival;
    private int tiroUsuario;

    Craps c = new Craps();
    Dado Dado11 = new Dado();
    Dado Dado22 = new Dado();
    DadoImagen d = new DadoImagen();
    DadoImagen e = new DadoImagen();
    DadoImagen f = new DadoImagen();
    DadoImagen g = new DadoImagen();

    int craps;
    int gano;
    int tirada;
    int suma;
    int ganadas = 0, perdidas = 0;

    public Juego(final NetConnection connection, String user) {
        this.connection = connection;
        this.usuario = user;
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
        BloquearTextos();
        labelUsuario.setText(usuario);
        txtPerdidas.setText("0");
        txtGanadas.setText("0");
        esperarJuego();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                connection.closeComunication();
                System.exit(0);
            }
        });
    }

    private void esperarJuego() {
        txtDado1.setText("");
        txtDado2.setText("");
        txtResultadoTirada.setText("");
        txtResultadoTirada2.setText("");
        labelEstado.setText("En espera...");
        labelRival.setText("");
        txtPerdidas.setText(String.valueOf(perdidas));
        txtGanadas.setText(String.valueOf(ganadas));
        jLDado1.setIcon(e.dadogif(3));
        jLDado2.setIcon(d.dadogif(4));
        btnTirar.setVisible(false);
        btnJugar.setEnabled(true);
        btnReiniciar.setEnabled(false);
        tiroUsuario = -1;
        tiroRival = -1;
    }

    private void esperarTiro() {
        labelEstado.setText("Esperando tiro de: " + usuarioRival);
        labelRival.setText(usuarioRival);
        btnTirar.setVisible(true);
        btnTirar.setEnabled(true);
        new HiloEsperarTiro().start();
    }

    void Limpiar() {// Método para reiniciar la partida
        int opcion = 3;
        int opcion2 = 4;
        jLDado1.setIcon(e.dadogif(opcion));
        jLDado2.setIcon(d.dadogif(opcion2));
        txtPerdidas.setText("");
        txtGanadas.setText("");
        txtDado2.setText("");
        txtResultadoTirada.setText("");
        txtDado1.setText("");
    }

    void BloquearTextos() {// Impide que el usuario mnodifique los JTexField
        btnTirar.setVisible(false);
        txtPerdidas.setEditable(false);
        txtGanadas.setEditable(false);
        txtDado2.setEditable(false);
        txtResultadoTirada.setEditable(false);
        txtDado1.setEditable(false);

    }

    void Tirar() {//<--- PARA método la tirar
        tiroUsuario = Dado11.GenerarDado();
        jLDado1.setIcon(f.Cara(tiroUsuario));
        String d1 = Integer.toString(tiroUsuario);
        txtDado1.setText(d1);
        txtResultadoTirada.setText(d1);

        PacketNet request_packet = new PacketNet();
        request_packet.setMensaje("Tiro," + tiroUsuario);
        request_packet.setTipo_mensaje(Protocol.ROUND);
        System.out.println("MANDANDO TIRO!!! :" + request_packet.getMensaje());
        connection.sendPacket(request_packet);
    }

    void calificarJuego() {

    }

    void Tirada(int numT) {// Si no gana numT pierde se repite este metodo hasta llegar Dado11 la tirada anterior

        if (tirada == 1) {
            JOptionPane.showMessageDialog(null, " Vuelve a sacar: " + numT
                    + ",Para ganar. Pero si sale 7 antes pierdes ");
            txtDado2.setText("");
            txtResultadoTirada.setText("");
            txtDado1.setText("");

            int selection = JOptionPane.showOptionDialog(
                    this,
                    "¿Tirar otra vez?", //Mensaje
                    "SELECCIONE UNA OPCION",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null, // null para icono por defecto.
                    new Object[]{"Si", "No"}, // Decision para seguir Jugando
                    "Si");
            if (selection == 0) {
                Tirar();
                int suma2 = suma;
                if (numT == suma2) {

                    perdidas++;
                    tirada = 0;
                    JOptionPane.showMessageDialog(null, "GANASTE");
                    txtGanadas.setText("" + perdidas);

                }
                if (suma2 == 7) {
                    ganadas++;
                    tirada = 0;
                    JOptionPane.showMessageDialog(null, "CRAPS PERDISTE");
                    txtPerdidas.setText("" + ganadas);

                } else {
                    Tirada(numT);

                }
            } else {
                ganadas++;
                JOptionPane.showMessageDialog(null, "Más suerte para la próxima");
                txtPerdidas.setText("" + ganadas);
            }

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jcMousePanel1 = new jcMousePanel.jcMousePanel();
        btnJugar = new javax.swing.JButton();
        jLDado2 = new javax.swing.JLabel();
        jLTitulo = new javax.swing.JLabel();
        jLDado1 = new javax.swing.JLabel();
        txtDado2 = new javax.swing.JTextField();
        txtResultadoTirada = new javax.swing.JTextField();
        txtDado1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtGanadas = new javax.swing.JTextField();
        btnTirar = new javax.swing.JButton();
        txtPerdidas = new javax.swing.JTextField();
        btnReiniciar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelEstado = new javax.swing.JLabel();
        txtResultadoTirada2 = new javax.swing.JTextField();
        labelUsuario = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        labelRival = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jcMousePanel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondojuego.jpg"))); // NOI18N
        jcMousePanel1.setMinimumSize(new java.awt.Dimension(700, 400));
        jcMousePanel1.setLayout(null);

        btnJugar.setBackground(new java.awt.Color(0, 204, 153));
        btnJugar.setFont(new java.awt.Font("Bookman Old Style", 3, 14)); // NOI18N
        btnJugar.setText("JUGAR");
        btnJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJugarActionPerformed(evt);
            }
        });
        jcMousePanel1.add(btnJugar);
        btnJugar.setBounds(390, 530, 137, 40);

        jLDado2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLDado2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/daIN2.png"))); // NOI18N
        jLDado2.setPreferredSize(new java.awt.Dimension(300, 300));
        jcMousePanel1.add(jLDado2);
        jLDado2.setBounds(540, 140, 300, 300);

        jLTitulo.setFont(new java.awt.Font("Berlin Sans FB Demi", 3, 24)); // NOI18N
        jLTitulo.setForeground(new java.awt.Color(153, 255, 255));
        jLTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLTitulo.setText("JUEGO DE DADOS");
        jcMousePanel1.add(jLTitulo);
        jLTitulo.setBounds(330, 10, 250, 40);

        jLDado1.setBackground(new java.awt.Color(255, 255, 255));
        jLDado1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLDado1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/daIN1.png"))); // NOI18N
        jLDado1.setPreferredSize(new java.awt.Dimension(300, 300));
        jcMousePanel1.add(jLDado1);
        jLDado1.setBounds(50, 140, 300, 300);

        txtDado2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtDado2.setForeground(new java.awt.Color(255, 0, 0));
        txtDado2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtDado2);
        txtDado2.setBounds(630, 440, 140, 40);

        txtResultadoTirada.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtResultadoTirada.setForeground(new java.awt.Color(255, 0, 0));
        txtResultadoTirada.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtResultadoTirada);
        txtResultadoTirada.setBounds(180, 100, 140, 30);

        txtDado1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtDado1.setForeground(new java.awt.Color(255, 0, 0));
        txtDado1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtDado1);
        txtDado1.setBounds(130, 440, 140, 40);

        jLabel4.setFont(new java.awt.Font("Bookman Old Style", 3, 20)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 255, 255));
        jLabel4.setText("Tu tirada es:");
        jcMousePanel1.add(jLabel4);
        jLabel4.setBounds(20, 110, 190, 20);

        txtGanadas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtGanadas.setForeground(new java.awt.Color(0, 0, 255));
        txtGanadas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtGanadas);
        txtGanadas.setBounds(360, 110, 70, 30);

        btnTirar.setBackground(new java.awt.Color(0, 204, 153));
        btnTirar.setFont(new java.awt.Font("Bookman Old Style", 3, 14)); // NOI18N
        btnTirar.setText("TIRAR");
        btnTirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTirarActionPerformed(evt);
            }
        });
        jcMousePanel1.add(btnTirar);
        btnTirar.setBounds(670, 530, 137, 40);

        txtPerdidas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtPerdidas.setForeground(new java.awt.Color(0, 0, 255));
        txtPerdidas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtPerdidas);
        txtPerdidas.setBounds(480, 110, 70, 30);

        btnReiniciar.setBackground(new java.awt.Color(0, 204, 153));
        btnReiniciar.setFont(new java.awt.Font("Bookman Old Style", 3, 14)); // NOI18N
        btnReiniciar.setText("REINICIAR");
        btnReiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReiniciarActionPerformed(evt);
            }
        });
        jcMousePanel1.add(btnReiniciar);
        btnReiniciar.setBounds(90, 530, 137, 40);

        jLabel5.setFont(new java.awt.Font("Berlin Sans FB Demi", 3, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(153, 255, 255));
        jLabel5.setText("PERDIDAS:");
        jcMousePanel1.add(jLabel5);
        jLabel5.setBounds(460, 80, 120, 30);

        jLabel7.setFont(new java.awt.Font("Berlin Sans FB Demi", 3, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(153, 255, 255));
        jLabel7.setText("GANADAS:");
        jcMousePanel1.add(jLabel7);
        jLabel7.setBounds(350, 80, 110, 30);

        labelEstado.setFont(new java.awt.Font("Berlin Sans FB Demi", 1, 16)); // NOI18N
        labelEstado.setForeground(new java.awt.Color(153, 255, 255));
        jcMousePanel1.add(labelEstado);
        labelEstado.setBounds(340, 440, 200, 30);

        txtResultadoTirada2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtResultadoTirada2.setForeground(new java.awt.Color(255, 0, 0));
        txtResultadoTirada2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jcMousePanel1.add(txtResultadoTirada2);
        txtResultadoTirada2.setBounds(580, 100, 140, 30);

        labelUsuario.setFont(new java.awt.Font("Bookman Old Style", 3, 20)); // NOI18N
        labelUsuario.setForeground(new java.awt.Color(153, 255, 255));
        labelUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        jcMousePanel1.add(labelUsuario);
        labelUsuario.setBounds(180, 80, 140, 20);

        jLabel8.setFont(new java.awt.Font("Bookman Old Style", 3, 20)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(153, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Su tirada es");
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        jcMousePanel1.add(jLabel8);
        jLabel8.setBounds(690, 110, 190, 20);

        labelRival.setFont(new java.awt.Font("Bookman Old Style", 3, 20)); // NOI18N
        labelRival.setForeground(new java.awt.Color(153, 255, 255));
        labelRival.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelRival.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        jcMousePanel1.add(labelRival);
        labelRival.setBounds(580, 80, 140, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jcMousePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jcMousePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnJugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJugarActionPerformed

        btnJugar.setEnabled(false);
        new HiloEsperarJugador().start();


        /*BloquearTextos();
        int opcion3 = 1;
        int opcion4 = 2;
        btnTirar.setVisible(true);
        jLDado1.setIcon(e.dadogif(opcion3));
        jLDado2.setIcon(d.dadogif(opcion4));*/

    }//GEN-LAST:event_btnJugarActionPerformed

    private void btnTirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTirarActionPerformed
        BloquearTextos();
        //btnJugar.setVisible(false);
        //btnTirar.setVisible(true);
        btnTirar.setEnabled(false);
        Tirar();
        /*craps = c.Crapso(suma);
        gano = c.Gana(suma);
        tirada = c.Tirada(suma);
        Tirada(suma);
        if (craps == 1) {
            ganadas++;
            txtPerdidas.setText("" + ganadas);
            JOptionPane.showMessageDialog(null, "CRAPS");
        }
        if (gano == 1) {
            perdidas++;
            txtGanadas.setText("" + perdidas);
            JOptionPane.showMessageDialog(null, "GANASTE");
        }*/


    }//GEN-LAST:event_btnTirarActionPerformed

    private void btnReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReiniciarActionPerformed
        /*BloquearTextos();
        
        ganadas = 0;
        perdidas = 0;
        Limpiar();*/
        esperarJuego();

    }//GEN-LAST:event_btnReiniciarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnJugar;
    private javax.swing.JButton btnReiniciar;
    private javax.swing.JButton btnTirar;
    private javax.swing.JLabel jLDado1;
    private javax.swing.JLabel jLDado2;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private jcMousePanel.jcMousePanel jcMousePanel1;
    private javax.swing.JLabel labelEstado;
    private javax.swing.JLabel labelRival;
    private javax.swing.JLabel labelUsuario;
    private javax.swing.JTextField txtDado1;
    private javax.swing.JTextField txtDado2;
    private javax.swing.JTextField txtGanadas;
    private javax.swing.JTextField txtPerdidas;
    private javax.swing.JTextField txtResultadoTirada;
    private javax.swing.JTextField txtResultadoTirada2;
    // End of variables declaration//GEN-END:variables

    class HiloEsperarJugador extends Thread {

        @Override
        public void run() {
            PacketNet request_packet = new PacketNet();
            labelEstado.setText("Esperando a otro jugador...");
            request_packet.setMensaje("LISTO PARA JUGAR");
            request_packet.setTipo_mensaje(Protocol.PLAY);
            connection.sendPacket(request_packet);
            PacketNet received_packet = connection.readPacket();
            if (received_packet.getTipo_mensaje().equals(Protocol.REQUEST_ACEPTED)) {
                System.out.println("RECIBIENDO1: " + received_packet.getMensaje());
                usuarioRival = received_packet.getMensaje().split(",")[1];
                esperarTiro();
            }
        }
    }

    class HiloEsperarTiro extends Thread {

        @Override
        public void run() {

            while (true) {
                PacketNet received_packet = connection.readPacket();
                if (received_packet == null) {
                    connection.closeComunication();
                    System.out.println("Sesion terminada del usuario");
                }
                if (received_packet.getTipo_mensaje() == null) {
                    //System.out.println("NADA");
                    continue;
                }
                System.out.println(received_packet.getTipo_mensaje());
                System.out.println(received_packet.getMensaje());
                //System.out.println("MSG: " + received_packet.getMensaje());
                if (received_packet.getTipo_mensaje().equals(Protocol.ROUND)) {
                    //System.out.println("RECIBIENDO: " + received_packet.getMensaje());
                    String msg = received_packet.getMensaje().split(",")[1];
                    tiroRival = Integer.parseInt(msg.trim());
                    labelEstado.setText(usuarioRival + " ya ha tirado.");
                    new HiloCalificarJuego().start();
                    break;
                }
            }
        }
    }

    class HiloCalificarJuego extends Thread {

        @Override
        public void run() {
            while (tiroRival == -1 || tiroUsuario == -1) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            txtDado2.setText(String.valueOf(tiroRival));
            txtResultadoTirada2.setText(String.valueOf(tiroRival));
            jLDado2.setIcon(g.Cara(tiroRival));

            if (tiroRival > tiroUsuario) {
                perdidas++;
                JOptionPane.showMessageDialog(null, "PERDISTE");
            } else if (tiroUsuario > tiroRival) {
                ganadas++;
                JOptionPane.showMessageDialog(null, "GANASTE");
            } else {
                JOptionPane.showMessageDialog(null, "EMPATASTE");
            }
            txtPerdidas.setText(String.valueOf(perdidas));
            txtGanadas.setText(String.valueOf(ganadas));
            btnReiniciar.setEnabled(true);
        }
    }

}
